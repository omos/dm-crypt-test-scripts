# Testing scripts from dm-crypt and dm-integrity

This repository contains some crude framework for testing the `dm-crypt` and `dm-integrity` kernel modules.

## Prerequisites

 * QEMU
 * a clean Linux kernel source tree (it will be built out-of-tree)
 * whatever I forgot to put here...

## Usage

### Initial setup

1. Clone the repository and change to its root directory.

2. Make sure the `kernel_src` directory contains a (clean!) Linux kernel source tree. It can be just a symlink to your existing cloned repository:

        ln -s /path/to/kernel/source kernel_src

3. Run `./init.sh` to download & build dependencies, to generate some testing data, etc.

4. That's it!

### (Re-)configuring the kernel

Run `./kernel_configure` and set all the config options to whatever you want/need. NOTE: Just make sure to disable loadable kernel modules as this doesn't work with eudyptula-boot for some reason... Also, you have to enable the following options:

    CONFIG_OVERLAY_FS=y
    CONFIG_9P_FS=y
    CONFIG_NET_9P=y
    CONFIG_NET_9P_VIRTIO=y
    CONFIG_VIRTIO=y
    CONFIG_VIRTIO_PCI=y
    CONFIG_VIRTIO_CONSOLE=y

You can also copy some existing `.config` file into the `kernel_build` directory to use an existing configuration.

### Building the kernel

Just run `./kernel_build.sh`.

### Starting the VM

Run `./start_eudyptula.sh`. This will start a minimal VM with the built kernel and give you a shell. The VM will have the host's root filesystem mapped via OverlayFS, so be careful!

You can pass additional eudyptula options to `./start_eudyptula.sh`. For example, if you want to have the host filesystem mounted as read-only and to use 2 GiB of virtual RAM, run `./start_eudyptula.sh -o -m 2G`. For more info on eudyptula-boot options run `./eudyptula-boot.sh --help`.

By default, the VM is started with 1GiB RAM, a 128MiB virtual HDD (mapped to a file under `/tmp`) and with the shell's working directory set to the `root` subdirectory.

To terminate the VM just exit the shell.

### Running dm-crypt/dm-integrity test scripts

Inside the VM shell, run one of the following scripts:

 * `./speed_dmcrypt.sh`: measures the performance of a dm-crypt device (mapped over a RAM disk)
 * `./test_dmcrypt.sh check`: checks correct en-/decryption of some reference images
 * `./test_dmintegrity.sh`: checks basic functionality of various dm-crypt/dm-integrity configurations

### Debugging the kernel (using gdb)

With the VM running, run `./start_gdb.sh`.

### Debugging the kernel (via an IDE)

With the VM running, run `./map_gdb_socket_to_tcp.sh`. Then you can connect to a remote GDB server at `localhost:6666` in your favorite IDE. Don't forget to use `kernel_build/vmlinux` as the binary to be debugged.
