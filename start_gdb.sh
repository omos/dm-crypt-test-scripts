#!/bin/bash

SOCKET="$(ls work/*-gdb.pipe)"

gdb -ex "target remote | socat UNIX:'$SOCKET' -" kernel_build/vmlinux
