#!/bin/bash

[ -d kernel_src ] || echo '[WARN] No kernel sources found! Make sure to run "ln -s /path/to/kernel/sources kernel_src" at some point.'

echo '[INFO] Creating some working subdirectories...'
mkdir kernel_build || exit 1
mkdir kernel_install || exit 1
mkdir work || exit 1

cd root || exit 1

echo '[INFO] Cloning dm_int_tools from GitHub...'
git clone 'https://github.com/mbroz/dm_int_tools.git' || exit 1

echo '[INFO] Building dm_int_tools...'
(cd dm_int_tools && make) || exit 1

echo '[INFO] Building reference dm-crypt images (using host kernel)...'
./test_dmcrypt.sh build || echo '[WARN] Unable to build reference dm-crypt images!'

