#!/bin/bash

SECTOR_SIZE=$1

BLOCK_SIZE=$((4*1024*1024))

if [ -z "$SECTOR_SIZE" ]; then
    SECTOR_OPTS=""
else
    SECTOR_OPTS="1 sector_size:$SECTOR_SIZE"
fi

function test_speed() {
    DEVICE="$1"
    SPEC="$2"
    KEY_SIZE="$3"
    
    SIZE=$(blockdev --getsize64 "$DEVICE")
    
    # Generate a key:
    if [ $KEY_SIZE -eq 0 ]; then
        KEY='-'
    else
        KEY="$(head -c $KEY_SIZE /dev/urandom | hexdump -v -e '/1 "%02X"')"
    fi
    
    dmsetup create tmp --table "0 $(($SIZE / 512)) crypt $SPEC $KEY 0 $DEVICE 0 $SECTOR_OPTS" || {
        return 1;
    }
    
    #echo "[INFO] Trying hdparm -tT --direct..."
    #hdparm -Tt --direct /dev/mapper/tmp
    
    echo "[INFO] Writing to $DEVICE, cipher '$SPEC', $KEY_SIZE-byte key..."
    dd if=/dev/zero of=/dev/mapper/tmp count=$(($SIZE / $BLOCK_SIZE)) bs=$BLOCK_SIZE oflag=direct
    
    echo "[INFO] Reading from $DEVICE, cipher '$SPEC', $KEY_SIZE-byte key..."
    dd if=/dev/mapper/tmp of=/dev/null count=$(($SIZE / $BLOCK_SIZE)) bs=$BLOCK_SIZE iflag=direct
    
    udevadm settle && dmsetup remove tmp
    
    return 0
}

DEVICE=/dev/ram1

# Warmup:
test_speed $DEVICE 'cipher_null-ecb' 0 >/dev/null 2>&1

echo '===== NULL-ECB ====='
test_speed $DEVICE 'cipher_null-ecb' 0
test_speed $DEVICE 'cipher_null-ecb' 0
echo

# This doesn't work - cipher_null has wrong blocksize
#echo '===== NULL-XTS ====='
#test_speed $DEVICE 'cipher_null-xts-plain64' 0
#test_speed $DEVICE 'cipher_null-xts-plain64' 0
#echo

echo '===== AES-ECB ====='
test_speed $DEVICE 'aes-ecb' 16
test_speed $DEVICE 'aes-ecb' 32
echo
echo '===== AES-CBC ====='
test_speed $DEVICE 'aes-cbc-plain64' 16
test_speed $DEVICE 'aes-cbc-plain64' 32
echo
echo '===== AES-CBC-ESSIV ====='
test_speed $DEVICE 'aes-cbc-essiv:sha256' 16
test_speed $DEVICE 'aes-cbc-essiv:sha256' 32
echo
echo '===== AES-CTR ====='
test_speed $DEVICE 'aes-ctr-plain64' 16
test_speed $DEVICE 'aes-ctr-plain64' 32
echo
echo '===== AES-XTS ====='
test_speed $DEVICE 'aes-xts-plain64' 32
test_speed $DEVICE 'aes-xts-plain64' 64
