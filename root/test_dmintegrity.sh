#!/bin/bash

function test_dmsetup() {
    DEVICE="$1"
    SIZE="$2"
    SPEC="$3"
    KEY_SIZE="$4"
    INTEGRITY_TYPE="$5"
    IV_SIZE="$6"
    TAG_SIZE="$7"
    
    echo "[INFO] Trying $SPEC with $KEY_SIZE-byte key, $IV_SIZE-byte IV, $TAG_SIZE-byte tag"
    
    INTEGRITY_SIZE=$(( $IV_SIZE + $TAG_SIZE ))

    # Generate a key:
    if [ $KEY_SIZE -eq 0 ]; then
        KEY='-'
    else
        KEY="$(head -c $KEY_SIZE /dev/urandom | hexdump -v -e '/1 "%02X"')"
    fi
    
    # Overwrite possible leftover superblock:
    dd if=/dev/zero of="$DEVICE" count=1 bs=1024 status=none
    
    dmsetup create x --table "0 $SIZE integrity $DEVICE 0 $INTEGRITY_SIZE J 0" || return 1
    dmsetup create y --table "0 $SIZE crypt $SPEC $KEY 0 /dev/mapper/x 0 1 integrity:$INTEGRITY_SIZE:$INTEGRITY_TYPE" || { dmsetup remove x; return 1; }
    
    dm_int_tools/integrity_check format /dev/mapper/y || { dmsetup remove y; dmsetup remove x; return 1; }
    
    echo "[INFO] Trying to write to the device..."
    dd if=/dev/urandom of=/tmp/stuff count=1 bs=$(blockdev --getsize64 /dev/mapper/y) status=none
    dd if=/tmp/stuff of=/dev/mapper/y status=none
    echo "[INFO] Trying to read from the device..."
    diff -u <(hexdump /tmp/stuff) <(hexdump /dev/mapper/y) | head -n 30
    
    echo "[INFO] Deactivating devices..."
    dmsetup remove y || return 1
    dmsetup remove x || return 1
}

DEV=/dev/hda
SIZE_INT=$((8*1024))

# maybe:
# notag(xts(aes)) : aead
# ivplain64(notag(xts(aes))) : aead
# ivrandom(gcm(aes)) : aead
# ivessiv(notag(gcm(aes))) : aead

# PROBLEM: how will iv*(...) handle authenc key???

# ---
# TODO: what if someone uses e.g. aes-xts-essiv/lmk/tcw? (if we fix them to CBC mode), though Milan is probably OK with it...
# EXAMPLES:
#   aes:64-cbc-lmk          -> notag(lmk(aes))-plain64 / lmk(cbc(aes))-plain64 [implies 64 sub-keys]
#   aes-cbc-tcw             -> notag(tcw(aes))-plain64 / tcw(cbc(aes))-plain64 [will bite off 32 bytes from the end of key]
#   aes-cbc-essiv:sha256    -> notag(essiv(aes,sha256,cbc(aes)))-plain64
#   aes-xts-plain64         -> notag(xts(aes))-plain64
#   aes-xts-benbi           -> notag(xts(aes))-benbi
#   aes-xts-random          -> notag(xts(aes))-random
# AEAD:
#   gcm(aes)-random
#   authenc(cmac(aes),cbc(aes))-random
#   [crazy, but these shall also work:]
#   authenc(cmac(aes),essiv(aes,sha256,cbc(aes)))-random [here we will parse the key as: CMAC key | AES key; essiv will hash the AES key]
#   authenc(cmac(aes),lmk(aes))-random [here we will parse the key as: CMAC key | 64*AES key]
#   authenc(cmac(aes),tcw(aes))-random [here we will parse the key as: CMAC key | AES key | TCW stuff]


##### HMAC #####
# full tag:
test_dmsetup "$DEV" "$SIZE_INT" 'capi:authenc(hmac(sha256),ctr(aes))-random' $((32 + 32)) 'aead' 16 32 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'capi:authenc(hmac(sha256),cbc(aes))-random' $((32 + 32)) 'aead' 16 32 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'capi:authenc(hmac(sha1),cbc(aes))-random' $((20 + 32)) 'aead' 16 20 || exit 1

# truncated tag:
test_dmsetup "$DEV" "$SIZE_INT" 'capi:authenc(hmac(sha256),cbc(aes))-random' $((32 + 32)) 'aead' 16 16 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'capi:authenc(hmac(sha1),cbc(aes))-random' $((20 + 32)) 'aead' 16 16 || exit 1

# XTS:
test_dmsetup "$DEV" "$SIZE_INT" 'capi:authenc(hmac(sha256),xts(aes))-random' $((32 + 32)) 'aead' 16 32 || exit 1

##### CMAC #####
test_dmsetup "$DEV" "$SIZE_INT" 'capi:authenc(cmac(aes),cbc(aes))-random' $((16 + 32)) 'aead' 16 16 || exit 1

##### GCM #####
test_dmsetup "$DEV" "$SIZE_INT" 'capi:gcm(aes)-random' 32 'aead' 12 16 || exit 1

# old format:
test_dmsetup "$DEV" "$SIZE_INT" 'aes-gcm-random' 32 'aead' 12 16 || exit 1

##### CCM #####
test_dmsetup "$DEV" "$SIZE_INT" 'capi:rfc4309(ccm(aes))-random' $((32 + 3)) 'aead' 8 16 || exit 1


##### ChaCha-Poly #####
test_dmsetup "$DEV" "$SIZE_INT" 'capi:rfc7539(chacha20,poly1305)-random' 32 'aead' 12 16 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'capi:rfc7539(ctr(aes),poly1305)-random' 32 'aead' 12 16 || exit 1

##### XTS without AEAD #####
test_dmsetup "$DEV" "$SIZE_INT" 'capi:xts(aes)-random' 32 'none' 16 0 || exit 1

# old format:
test_dmsetup "$DEV" "$SIZE_INT" 'aes-xts-random' 32 'none' 16 0 || exit 1

##### CTR without AEAD #####
test_dmsetup "$DEV" "$SIZE_INT" 'capi:ctr(aes)-random' 32 'none' 16 0 || exit 1

##### Legacy format #####
test_dmsetup "$DEV" "$SIZE_INT" 'aes-plain' 32 'none' 1 0 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'aes' 32 'none' 1 0 || exit 1

# from cryptsetup tests:
test_dmsetup "$DEV" "$SIZE_INT" 'cipher_null-ecb' 0 'none' 1 0 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'cipher_null-ecb-plain' 0 'none' 1 0 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'aes-ecb' 32 'none' 1 0 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'aes-ecb-plain' 32 'none' 1 0 || exit 1

# both plain and plain64 should work:
test_dmsetup "$DEV" "$SIZE_INT" 'capi:xts(aes)-plain' 32 'none' 1 0 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'capi:chacha20-plain' 32 'none' 1 0 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'capi:xts(aes)-plain64' 32 'none' 1 0 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'capi:chacha20-plain64' 32 'none' 1 0 || exit 1

# essiv format:
test_dmsetup "$DEV" "$SIZE_INT" 'capi:cbc(aes)-essiv:sha256' 32 'none' 1 0 || exit 1
test_dmsetup "$DEV" "$SIZE_INT" 'aes-cbc-essiv:sha256' 32 'none' 1 0 || exit 1
