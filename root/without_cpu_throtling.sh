#!/bin/bash

##### CPU throttling control #####

function save_and_set() {
    local var="$1"; shift
    local file="$1"; shift
    local value="$1"; shift
    
    [ -f "$file" ] || return
    
    declare -g $var="$(cat "$file")"
    
    echo "$value" >"$file"
}

function restore() {
    local var="$1"; shift
    local file="$1"; shift
    
    [ -f "$file" ] || return
    
    echo "${!var}" >"$file"
    
    unset "$var"
}

function cpu_throttling_disable() {
    save_and_set CPUT_INTEL_NO_TURBO /sys/devices/system/cpu/intel_pstate/no_turbo 1
    save_and_set CPUT_CPUFREQ_BOOST  /sys/devices/system/cpu/cpufreq/boost 0
    
    local cpu_id=0
    while [ $cpu_id -lt 1000 ]; do
        local cpu_path="/sys/devices/system/cpu/cpu$cpu_id"
        [ -d "$cpu_path" ] || break
        
        save_and_set CPUT_CPU${cpu_id}_CPUFREQ_GOVERNOR \
            "$cpu_path/cpufreq/scaling_governor" performance
        
        local state_id=1
        while [ $state_id -lt 1000 ]; do
            local state_path="$cpu_path/cpuidle/state$state_id"
            [ -d "$state_path" ] || break
            
            save_and_set CPUT_CPU${cpu_id}_IDLE_STATE${state_id} \
                "$state_path/disable" 1
            
            (( state_id++ ))
        done
        
        (( cpu_id++ ))
    done
}

function cpu_throttling_restore() {
    restore CPUT_INTEL_NO_TURBO /sys/devices/system/cpu/intel_pstate/no_turbo
    restore CPUT_CPUFREQ_BOOST  /sys/devices/system/cpu/cpufreq/boost
    
    local cpu_id=0
    while [ $cpu_id -lt 1000 ]; do
        local cpu_path="/sys/devices/system/cpu/cpu$cpu_id"
        [ -d "$cpu_path" ] || break
        
        restore CPUT_CPU${cpu_id}_CPUFREQ_GOVERNOR \
            "$cpu_path/cpufreq/scaling_governor"
        
        local state_id=1
        while [ $state_id -lt 1000 ]; do
            local state_path="$cpu_path/cpuidle/state$state_id"
            [ -d "$state_path" ] || break
            
            restore CPUT_CPU${cpu_id}_IDLE_STATE${state_id} \
                "$state_path/disable"
            
            (( state_id++ ))
        done
        
        (( cpu_id++ ))
    done
}

if [ $UID -ne 0 ]; then
    echo "ERROR: This script must be run as root!" 1>&2
    exit 1
fi

cpu_throttling_disable && trap cpu_throttling_restore EXIT

"$@"
