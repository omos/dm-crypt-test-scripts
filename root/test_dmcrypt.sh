#!/bin/bash

function create_image() {
    local PAYLOAD="$1"
    local IMAGE="$2"
    local KEY="$3"
    local SPEC="$4"
    
    local SIZE=$(du -b $PAYLOAD | cut -f1)
    
    dd if=/dev/zero of="$IMAGE" count=1 bs=$SIZE status=none
    
    sudo losetup /dev/loop0 "$IMAGE" || { return 1; }
    
    sudo dmsetup create tmp --table "0 $(($SIZE / 512)) crypt $SPEC $KEY 0 /dev/loop0 0" || {
        sudo losetup -d /dev/loop0; return 1;
    }
    
    sudo dd if=$PAYLOAD of=/dev/mapper/tmp count=1 bs=$SIZE status=none
    local RET=$?
    
    sudo losetup -d /dev/loop0
    udevadm settle && sudo dmsetup remove tmp
    
    return $RET
}

function check_image() {
    local PAYLOAD="$1"
    local IMAGE="$2"
    local KEY="$3"
    local SPEC="$4"
    
    local SIZE=$(du -b $PAYLOAD | cut -f1)
    
    sudo losetup /dev/loop0 "$IMAGE" || { return 1; }
    
    sudo dmsetup create tmp --table "0 $(($SIZE / 512)) crypt $SPEC $KEY 0 /dev/loop0 0" || {
        sudo losetup -d /dev/loop0; return 1;
    }
    
    sudo diff -q "$PAYLOAD" /dev/mapper/tmp || sudo diff -u <(hd "$PAYLOAD") <(hd /dev/mapper/tmp)
    local RET=$?
    
    sudo losetup -d /dev/loop0
    udevadm settle && sudo dmsetup remove tmp
    
    return $RET
}

function verify_image() {
    local NAME="$1"
    local SPEC="$2"
    
    local PAYLOAD="$NAME-ptxt.bin"
    local IMAGE="$NAME-ctxt.bin"
    local KEYFILE="$NAME-key.bin"
    
    local KEY="$(cat "$KEYFILE")"
    
    echo "[INFO] $NAME: Checking correct decryption..."
    check_image "$PAYLOAD" "$IMAGE" "$KEY" "$SPEC" || {
        echo "[ERROR] Check failed!";
    }
    
    local TEMP="$(mktemp)"
    
    echo "[INFO] $NAME: Checking correct encryption..."
    create_image "$PAYLOAD" "$TEMP" "$KEY" "$SPEC"
    
    diff -q "$TEMP" "$IMAGE" || diff -u <(hd "$TEMP") <(hd "$IMAGE") || {
        echo "[ERROR] Check failed!";
    }
    
    rm -f "$TEMP"
}

function build_image() {
    local NAME="$1"
    local SPEC="$2"
    local SIZE="$3"
    local KEY_SIZE="$4"
    
    local PAYLOAD="$NAME-ptxt.bin"
    local IMAGE="$NAME-ctxt.bin"
    local KEYFILE="$NAME-key.bin"
    
    # Generate a key:
    if [ $KEY_SIZE -eq 0 ]; then
        local KEY='-'
    else
        local KEY="$(head -c $KEY_SIZE /dev/urandom | hexdump -v -e '/1 "%02X"')"
    fi
    
    echo -n "$KEY" > "$KEYFILE"
    
    echo "[INFO] $NAME: Building image..."
    
    # write the payload:
    dd if=/dev/urandom of="$PAYLOAD" count=1 bs="$SIZE" status=none
    
    # allocate the image:
    dd if=/dev/zero of="$IMAGE" count=1 bs="$SIZE" status=none
    
    create_image "$PAYLOAD" "$IMAGE" "$KEY" "$SPEC"
}

ACTION="$1"
SIZE=16M

case "$ACTION" in
    'build')
        CMD=build_image
        ;;
    'check')
        CMD=verify_image
        ;;
    *)
        echo "ERROR: Invalid action: $ACTION" 1>&2
        exit 1
        ;;
esac

"$CMD" 'loopaes-128-v2' 'aes:64-cbc-lmk' $SIZE $((64 * 16))
"$CMD" 'loopaes-128-v3' 'aes:64-cbc-lmk' $SIZE $((65 * 16))
"$CMD" 'loopaes-256-v2' 'aes:64-cbc-lmk' $SIZE $((64 * 32))
"$CMD" 'loopaes-256-v3' 'aes:64-cbc-lmk' $SIZE $((65 * 32))

"$CMD" 'tcrypt-128' 'aes-cbc-tcw' $SIZE $((16 + 16 + 16))
"$CMD" 'tcrypt-256' 'aes-cbc-tcw' $SIZE $((32 + 16 + 16))

"$CMD" 'aes-ecb-256'      'aes-ecb'      $SIZE 32
"$CMD" 'aes-cbc-plain64-256'      'aes-cbc-plain64'      $SIZE 32
"$CMD" 'aes-ctr-benbi-256'        'aes-ctr-benbi'        $SIZE 32
"$CMD" 'aes-xts-plain64-256'      'aes-xts-plain64'      $SIZE 32
"$CMD" 'aes-cbc-essiv-sha256-256' 'aes-cbc-essiv:sha256' $SIZE 32
