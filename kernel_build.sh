#!/bin/bash

make -C kernel_src O=$(readlink -f kernel_build) -j4 || exit 1
make -C kernel_src O=$(readlink -f kernel_build) install INSTALL_PATH=$(readlink -f kernel_install)
