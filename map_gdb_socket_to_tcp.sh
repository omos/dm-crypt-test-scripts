#!/bin/bash

SOCKET="$(ls work/*-gdb.pipe)"

socat tcp-l:6666,reuseaddr,fork unix:"$SOCKET"
