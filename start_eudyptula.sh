#!/bin/bash

cd root || exit 1

TMP="$(mktemp -d)"
trap "rm -rf '$TMP'" EXIT

dd if=/dev/zero of="$TMP/disk0.img" bs=128M count=1 status=none

if [ -z "$KERNEL_VERSION" ]; then
    KERNEL_VERSION="$(make -C ../kernel_src kernelversion | grep -v 'make:')" || {
        echo 1>&2 "ERROR: Unable to determine kernel version!"
        exit 1
    }
fi

KERNEL_PATH="../kernel_install/vmlinuz-$KERNEL_VERSION"

[ -f "$KERNEL_PATH" ] || KERNEL_PATH="$KERNEL_PATH+"

../eudyptula-boot.sh -o -m 1G -d ../work -k "$KERNEL_PATH" \
    --qemu="-cpu host -drive file='$TMP/disk0.img',format=raw,media=disk" \
    $@
